# FindMyDevice

Find your Device via SMS.
This applications goal is to track your device when it's lost and should be a secure open source application and a alternative to Googles FindMyDevice.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/de.nulide.findmydevice/)


# Why?

I have lost my  brand new phone last week.
I have to say, if i would have used Googles Services like (FindMyDevice) i could have a better chance to find it.
But why the heck did nobody created an open source application to find a lost device without giving up your privacy and feeding the big data crake Google.

If you like and want to support this project, spread the info about this application or donate.

# Donate

<script src="https://liberapay.com/Nulide/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/Nulide/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>
